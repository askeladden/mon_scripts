#!/usr/bin/perl

use strict;
use warnings;
use Riemann::Client;
#use Data::Dumper;

#directory with executables monitors
my $directory = '/usr/lib/logstat/modules';

#lock against DOS
my $lock = '/dev/shm/execut.lock';

open (LOCK, ">", $lock) or die $!;
flock (LOCK, 2|4) or die $!;

#rieman connection details
##TO_DO: config via ansible
my $riemann_serv='10.133.157.232';
my $riemann_port='5555';

# array of hashes with metrics
my @events;

my $r = Riemann::Client->new(
    host => $riemann_serv,
    port => $riemann_port,
);

opendir (DIR, $directory) or die $!;
	foreach (grep { $_ !~ m/^\./ && -x "$directory/$_" } readdir DIR) {
		if (open EXEC, '-|', "$directory/$_") {
		    while ( <EXEC>) {
			  push @events, { map { split /=/, $_, 2 } split /\s+/ };
                    }
                close EXEC;
		}
	}
$r->send(@events);
closedir (DIR);
#print Dumper(\@events);
close (LOCK);
exit;


